package com.laolang.be.modules.sys.web.admin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.laolang.be.common.core.domain.R;
import com.laolang.be.common.core.util.LogUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Slf4j
@RequestMapping("admin/dashboard")
@RestController
public class DashboardAdminController {

    @GetMapping("info")
    public R<Boolean> info() {
    	LogUtil.info(log,"dashboard");
        return R.ok(true);
    }
}
