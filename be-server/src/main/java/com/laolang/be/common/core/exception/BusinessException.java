package com.laolang.be.common.core.exception;

import com.laolang.be.common.core.consts.StatusCodeConst;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final Integer code;

    private final String msg;

    public BusinessException(String msg) {
        this.code = StatusCodeConst.ERROR.getCode();
        this.msg = msg;
    }

    public BusinessException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(Integer code, String msg, String message) {
        super(message);
        this.code = code;
        this.msg = msg;
    }
}
