package com.laolang.be.common.data.cache.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Data
@Configuration
public class RedissonConfig {

    @Value(("${redisson.ip}"))
    private String ip;

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer().setAddress(ip);
        return Redisson.create(config);

    }
}
