package com.laolang.be.common.data.mvc.trace;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.laolang.be.common.data.cache.redisson.RedissonFunction;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@AllArgsConstructor
@Component
public class TraceComponent {

    private static final String AUTH_PREFIX = "/auth";
    private static final String ADMIN_PREFIX = "/admin";
    private static final String API_PREFIX = "/api";

    private static final String TRACE_ID_LOCK = "app_lock_trace_id";

    private final RedisTemplate<String, Object> redisTemplate;

    private final RedissonFunction redissonFunction;

    public synchronized String getNextTraceId(String uri, LocalDateTime time) {
        return redissonFunction.lock(TRACE_ID_LOCK, () -> {
            StringBuilder sb = new StringBuilder();
            if (StrUtil.startWith(uri, AUTH_PREFIX)) {
                sb.append("1");
            } else if (StrUtil.startWith(uri, ADMIN_PREFIX)) {
                sb.append("2");
            } else if (StrUtil.startWith(uri, API_PREFIX)) {
                sb.append("3");
            }
            sb.append(DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(time));
            String key = "trace_id_" + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(time);
            Object o = redisTemplate.opsForValue().get(key);
            if (null == o) {
                sb.append(1);
                redisTemplate.opsForValue().set(key, 2);
            } else {
                Integer id = Integer.parseInt(o.toString()) + 1;
                sb.append(id);
                redisTemplate.opsForValue().set(key, id);
            }
            redisTemplate.expire(key, 75, TimeUnit.SECONDS);
            return sb.toString();
        });

    }
}
