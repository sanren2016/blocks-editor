package com.laolang.be.common.core.util;

import org.slf4j.Logger;

import cn.hutool.core.util.StrUtil;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
public class LogUtil {

    private LogUtil() {
    }

    private static final int ORIGIN_STACK_INDEX = 2;

    // 模块名 流水号 类名 方法 行号 消息内容
    private static final String FORMAT = "[Business Log] [{}] ===> {}";

    /**
     * 获取当前文件名称
     */
    public static String getFileName() {
        return Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX].getFileName();
    }

    /**
     * 获取当前类名称
     */
    public static String getClassName() {
        return Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX].getClassName();
    }

    /**
     * 获取当前方法名称
     */
    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX].getMethodName();
    }

    /**
     * 获取当前代码行号
     */
    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX].getLineNumber();
    }

    /**
     * info 信息
     *
     * @param logger  logger
     * @param message 日志内容
     */
    public static void info(Logger logger, String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX];
        logger.info(getMessage(ste, message));
    }


    /**
     * error 信息
     *
     * @param logger  logger
     * @param message 日志内容
     */
    public static void error(Logger logger, String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX];
        logger.error(getMessage(ste, message));
    }

    /**
     * warn 信息
     *
     * @param logger  logger
     * @param message 日志内容
     */
    public static void warn(Logger logger, String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX];
        logger.warn(getMessage(ste, message));
    }

    private static String getMessage(StackTraceElement ste, String message) {
        return StrUtil.format(FORMAT, ste.getMethodName(), message);
    }


}
