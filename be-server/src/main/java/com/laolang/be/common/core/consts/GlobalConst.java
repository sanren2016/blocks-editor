package com.laolang.be.common.core.consts;
/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
public class GlobalConst {

    public static final Long ADMIN_ID = 1L;

    public enum CrudModel {
        /**
         *
         */
        ADD, EDIT, DELETE, QUERY
    }
}
