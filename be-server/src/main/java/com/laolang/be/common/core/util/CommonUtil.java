package com.laolang.be.common.core.util;

import java.util.List;

import com.laolang.be.common.core.consts.GlobalConst;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
public class CommonUtil {

    private CommonUtil() {
    }

    private static final String BEARER = "Bearer ";

    /* 常用正则表达式 */
    // 手机号
    public static final String REGEX_PHONE = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";

    // 密码 【同时包含字母和数字】
    public static final String REGEX_PASSWORD = "^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";


    /**
     * 取出token的 Bearer 头
     *
     * @param token token
     * @return 无 Bearer 头的 token
     */
    public static String getTokenWithoutBearer(String token) {
        return StrUtil.startWith(token, BEARER) ? StrUtil.sub(token, 7, token.length()) : token;
    }

    /**
     * 手机号脱敏
     *
     * @param phone 原手机号
     * @return 脱敏手机号码
     */
    public static String phoneMasking(String phone) {
        return StrUtil.sub(phone, 0, 3) +
                "****" +
                StrUtil.sub(phone, 7, phone.length());
    }

    /**
     * 根据 userId 判断是否为管理员用户, 管理员账号ID暂定为 1
     *
     * @param userId 登录账号id
     * @return 是否为管理员用户
     */
    public static boolean isAdmin(Long userId) {
        return GlobalConst.ADMIN_ID.equals(userId);
    }

    /**
     * 判断常量枚举值列表中是否包含某值
     *
     * @param clazz 枚举类
     * @param value 值
     * @return boolean
     */
    public static boolean enumHasValue(Class<? extends Enum<?>> clazz, Integer value) {
        boolean has = false;
        List<Object> valueObjects = EnumUtil.getFieldValues(clazz, "value");
        for (Object valueObject : valueObjects) {
            if (Integer.parseInt(valueObject.toString()) == value) {
                has = true;
                break;
            }
        }
        return has;
    }

    public static boolean isAddCrudModel(GlobalConst.CrudModel crudModel) {
        return GlobalConst.CrudModel.ADD == crudModel;
    }

    public static boolean isEditCrudModel(GlobalConst.CrudModel crudModel) {
        return GlobalConst.CrudModel.EDIT == crudModel;
    }

    public static boolean isDeleteCrudModel(GlobalConst.CrudModel crudModel) {
        return GlobalConst.CrudModel.DELETE == crudModel;
    }

    public static boolean isQueryCrudModel(GlobalConst.CrudModel crudModel) {
        return GlobalConst.CrudModel.QUERY == crudModel;
    }

    /**
     * 根据 uri 解析模块名
     *
     * @param uri 请求地址
     * @return 模块名
     */
    public static String getModuleName(String uri) {
        String[] split = StrUtil.split(uri, "/");
        if (ArrayUtil.isNotEmpty(split) && split.length >= 2) {
            return split[1];
        }
        return null;
    }
}
