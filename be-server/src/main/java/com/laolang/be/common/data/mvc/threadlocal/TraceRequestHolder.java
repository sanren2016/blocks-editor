package com.laolang.be.common.data.mvc.threadlocal;
/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
public class TraceRequestHolder {

    private final static ThreadLocal<String> REQUEST_HOLDER = new ThreadLocal<>();

    public static void set(String traceId) {
        REQUEST_HOLDER.set(traceId);
    }

    public static String get() {
        return REQUEST_HOLDER.get();
    }

    public static void remove() {
        REQUEST_HOLDER.remove();
    }

}
