package com.laolang.be.common.data.cache.redisson;

import java.util.function.Supplier;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import com.laolang.be.common.core.util.LogUtil;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Slf4j
@AllArgsConstructor
@Component
public class RedissonFunction {

    private final RedissonClient redisson;

    public <T> T lock(String lockKey, Supplier<T> supplier) {
        RLock lock = redisson.getLock(lockKey);
        T t = null;
        try {
            lock.lock();
            t = supplier.get();
        } catch (Exception e) {
            LogUtil.error(log, StrUtil.format("流水号加锁失败,msg:{}", ExceptionUtils.getMessage(e)));
        } finally {
            lock.unlock();
        }
        return t;
    }

}

