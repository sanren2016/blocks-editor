package com.laolang.be.common.data.mvc.interceptor;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.laolang.be.common.core.util.CommonUtil;
import com.laolang.be.common.core.util.LogUtil;
import com.laolang.be.common.data.mvc.threadlocal.TraceRequestHolder;
import com.laolang.be.common.data.mvc.trace.TraceComponent;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Slf4j
@AllArgsConstructor
@Component
public class TraceInterceptor implements HandlerInterceptor {

    private final TraceComponent traceComponent;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();

        String traceId = traceComponent.getNextTraceId(uri, LocalDateTime.now());
        TraceRequestHolder.set(traceId);
        MDC.put("traceId", traceId);
        MDC.put("module", CommonUtil.getModuleName(uri));
        LogUtil.info(log, StrUtil.format("request-begin:{}", uri));
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LogUtil.info(log, StrUtil.format("request-end:{}", request.getRequestURI()));
    }
}
