package com.laolang.be.common.data.mvc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.laolang.be.common.data.mvc.interceptor.TraceInterceptor;

import lombok.AllArgsConstructor;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@AllArgsConstructor
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final TraceInterceptor traceInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(traceInterceptor).addPathPatterns("/**");
    }
}
