package com.laolang.be.common.data.mvc.advice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.laolang.be.common.core.domain.R;
import com.laolang.be.common.core.exception.BusinessException;
import com.laolang.be.common.core.util.LogUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Slf4j
@ControllerAdvice
public class MvcExceptionHandle {

    /**
     * 业务报错
     */
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<R<String>> businessHandler(BusinessException e) {
        R<String> ajax = R.failed();
        ajax.setPropFromBusinessException(e);
        LogUtil.error(log, e.getMsg());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ajax);
    }


    /**
     * jsr 303
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<R<Map<String, String>>> jsr303Handler(MethodArgumentNotValidException e) {
        R<Map<String, String>> ajax = R.badRequest();
        BindingResult bindingResult = e.getBindingResult();
        if (bindingResult.hasErrors()) {
            Map<String, String> map = new HashMap<>();
            bindingResult.getFieldErrors().forEach(fieldError -> map.put(fieldError.getField(), fieldError.getDefaultMessage()));
            ajax.setBody(map);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ajax);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<R<String>> httpRequestMethodNotSupportedException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(R.notFount());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<R<String>> exceptionHandler(Exception e) {
        LogUtil.error(log, e.getMessage());
        R<String> ajax = R.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ajax);
    }
}
