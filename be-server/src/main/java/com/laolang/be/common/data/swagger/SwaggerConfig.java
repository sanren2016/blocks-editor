package com.laolang.be.common.data.swagger;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.SpringfoxWebMvcConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Configuration
@ConditionalOnClass(SpringfoxWebMvcConfiguration.class)
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean(value = "sysApi")
    public Docket platformApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(sysAiInfo())
                //分组名称
                .groupName("sys")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.laolang.be.modules.sys.web"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo sysAiInfo() {
        return new ApiInfoBuilder()
                .title("sys api")
                .description("sys api")
                .termsOfServiceUrl("http://localhost:8081/")
                .contact(new Contact("khlbat", "https://gitee.com/sanren2016/blocks-editor", "xiaodaima2016@163.com"))
                .version("1.0")
                .build();
    }
}

