package com.laolang.be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * @author khlbat
 * @version 1.0
 * @date 2021-02-28
 */
@Slf4j
@SpringBootApplication
public class BlocksEditorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlocksEditorApplication.class, args);
		log.info("Blocks Editor Server is running...");
	}
}
