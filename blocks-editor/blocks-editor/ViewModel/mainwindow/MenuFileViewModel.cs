﻿using blocks_editor.ui.dialog.wizard;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace blocks_editor.ViewModel.mainwindow
{
    public class MenuFileViewModel : ViewModelBase
    {
        public RelayCommand NewMavenProjectCmd { get; }

        public MenuFileViewModel()
        {
            NewMavenProjectCmd = new RelayCommand(OnNewMavenProjectCmd);
        }

        /// <summary>
        /// 新建 maven 项目
        /// </summary>
        private void OnNewMavenProjectCmd()
        {
            NewMavenProjectWizard wizard = new NewMavenProjectWizard();
            wizard.ShowDialog();
        }
    }
}
