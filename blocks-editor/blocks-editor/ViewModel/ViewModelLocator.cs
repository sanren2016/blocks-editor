/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:blocks_editor"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using blocks_editor.ViewModel.dialog;
using blocks_editor.ViewModel.mainwindow;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace blocks_editor.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            #region app
            SimpleIoc.Default.Register<WorkspaceViewModel>();
            SimpleIoc.Default.Register<SelectWorkingSetViewModel>();
            SimpleIoc.Default.Register<NewWorkingSetViewModel>();
            #endregion app

            #region Launcher
            SimpleIoc.Default.Register<LauncherViewModel>();
            #endregion Launcher

            #region MainWindow
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<MenuFileViewModel>();
            #endregion MainWindow
        }


        #region app
        public WorkspaceViewModel WorkspaceViewModel => ServiceLocator.Current.GetInstance<WorkspaceViewModel>();
        public SelectWorkingSetViewModel SelectWorkingSetViewModel => ServiceLocator.Current.GetInstance<SelectWorkingSetViewModel>();
        public NewWorkingSetViewModel NewWorkingSetViewModel => ServiceLocator.Current.GetInstance<NewWorkingSetViewModel>();
        #endregion app

        #region Launcher
        public LauncherViewModel LauncherViewModel => ServiceLocator.Current.GetInstance<LauncherViewModel>();
        #endregion Launcher

        #region MainWindow
        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public MenuFileViewModel MenuFileViewModel => ServiceLocator.Current.GetInstance<MenuFileViewModel>();
        #endregion MainWindow



        public static void Cleanup()
        {
        }
    }
}