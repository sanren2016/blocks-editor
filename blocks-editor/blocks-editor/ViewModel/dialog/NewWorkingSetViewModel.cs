﻿using blocks_editor.src.consts;
using blocks_editor.ui.dialog;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace blocks_editor.ViewModel.dialog
{
    public class NewWorkingSetViewModel : ViewModelBase
    {
        public NewWorkingSetViewModel()
        {
            Messenger.Default.Register<string>(this, MessengerConst.NewWorkingSet.OPEN_NEW_WORKING_SET, OpenMsg);
        }

        /// <summary>
        /// 打开选择 working set 对话框
        /// </summary>
        /// <param name="obj"></param>
        public void OpenMsg(string obj)
        {
            NewWorkingSetDialog dialog = new NewWorkingSetDialog();
            dialog.ShowDialog();
        }
    }
}
