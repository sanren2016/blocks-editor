﻿using blocks_editor.src.consts;
using blocks_editor.ui.dialog;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace blocks_editor.ViewModel.dialog
{
    public class SelectWorkingSetViewModel : ViewModelBase
    {
        public SelectWorkingSetViewModel()
        {
            Messenger.Default.Register<string>(this, MessengerConst.SelectWorkingSet.OPEN_SELECT_WORKING_SET, OpenMsg);
        }

        /// <summary>
        /// 打开选择 working set 对话框
        /// </summary>
        /// <param name="obj"></param>
        public void OpenMsg(string obj)
        {
            SelectWorkingSetDialog dialog = new SelectWorkingSetDialog();
            dialog.ShowDialog();
        }
    }
}
