﻿using blocks_editor.src.model;
using GalaSoft.MvvmLight;

namespace blocks_editor.ViewModel
{
    public class WorkspaceViewModel : ViewModelBase
    {

        public WorkspaceViewModel()
        {
            workspaceModel = new WorkspaceModel();
        }

        private WorkspaceModel workspaceModel;
        public WorkspaceModel WorkspaceModel
        {
            get { return workspaceModel; }
            set
            {
                workspaceModel = value;
            }
        }
    }
}
