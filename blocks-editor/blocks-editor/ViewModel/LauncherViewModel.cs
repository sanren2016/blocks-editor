﻿using System;
using System.Collections.ObjectModel;
using blocks_editor.src.consts;
using blocks_editor.src.model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System.Linq;
using System.Xml.Linq;
using blocks_editor.src.exception;
using blocks_editor.src.util;
using log4net;
using blocks_editor.src.dto;

namespace blocks_editor.ViewModel
{
    public class LauncherViewModel : ViewModelBase
    {
        readonly static ILog log = LogManager.GetLogger(typeof(LauncherViewModel));

        public LauncherViewModel()
        {

            InitData();
            LoadData();
            RegisterMessenger();
        }


        #region init

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void InitData()
        {
            itemList = new ObservableCollection<LauncherModel>();
            selectItem = new LauncherModel();
            lastId = 0;
            copySettingShow = false;
            userAsDefaultShow = true;
            launcherType = LauncherType.NEW;

            copyPerferences = false;

            root = XElement.Load(CommonConst.WORKSPACE_DATA_PATH);
        }

        /// <summary>
        /// 从文件加载工作空间记录
        /// </summary>
        private void LoadData()
        {
            try
            {
                lastId = Convert.ToInt32(root.Element("last").Attribute("id").Value);

                var els = from el in root.Elements("workspace") select el;
                if (els.Any())
                {
                    foreach (var el in els)
                    {
                        LauncherModel bean = new LauncherModel(el.Attribute("path").Value, Convert.ToInt32(el.Attribute("id").Value));
                        itemList.Add(bean);
                        if (lastId == bean.Id)
                        {
                            selectItem = bean;
                        }
                    }
                }
            }
            catch (BussinessException e)
            {
                log.Error(StrUtil.format("加载工作空间出错", e.Message));
            }
        }

        private void RegisterMessenger()
        {
            Messenger.Default.Register<string>(this, MessengerConst.Launcher.LAUNCH_WORKSPACE, LaunchWorkspaceMsg);
            Messenger.Default.Register<string>(this, MessengerConst.Launcher.SELECT_WORKSPACE, SelectWorkspaceMsg);
            Messenger.Default.Register<NotificationMessage<SwitchWorkspaceDto>>(this, MessengerConst.Launcher.CHANGE_LAUNCH_TYPE, ChangeLaunchTypeMsg);
        }



        #endregion init

        #region Messenger

        /// <summary>
        /// 选择工作空间目录
        /// </summary>
        /// <param name="obj"></param>
        private void SelectWorkspaceMsg(string selectedPath)
        {
            //log.Info(selectedPath);
            var result = from item in itemList where item.Path == selectedPath select item;
            if (!result.Any())
            {
                LauncherModel newItem = new LauncherModel(selectedPath, itemList.Count + 1);

                itemList.Add(newItem);
                selectItem = newItem;
            }
            else
            {
                selectItem = result.ToArray()[0];
            }
            lastId = selectItem.Id;
            RaisePropertyChanged("SelectItem");
        }

        /// <summary>
        /// 加载工作空间
        /// </summary>
        /// <param name="obj"></param>
        private void LaunchWorkspaceMsg(string obj)
        {
            /* 同步用户文件 */
            SyncUserData();

            /* 创建工作空间目录 */
            CreateWorkspaceFolder();

            /* 复制配置 */
            CopySettings();

            /* 打开主窗口 */
            Messenger.Default.Send(new NotificationMessage<OpenMainWindowDto>(new OpenMainWindowDto()
            {
                WorkspacePath = SelectItem.Path,
                LauncherType = launcherType,
                MainWindow = currMainWindow
            }, MessengerConst.App.APP_LAUNCH_MAIN_WINDOW), MessengerConst.App.APP_LAUNCH_MAIN_WINDOW);
        }

        /// <summary>
        /// 同步用户文件
        /// </summary>
        private void SyncUserData()
        {
            // 如果是新的工作空间, 则同步用户文件
            if (IsNewWorkspace())
            {
                root.Add(new XElement("workspace", new XAttribute("id", Convert.ToString(selectItem.Id)), new XAttribute("path", selectItem.Path)));
            }

            // 修改默认工作空间
            var els = from el in root.Elements("last") select el;
            els.ToArray()[0].SetAttributeValue("id", Convert.ToString(selectItem.Id));

            // 保存文件
            root.Save(CommonConst.WORKSPACE_DATA_PATH);
        }

        /// <summary>
        /// 判断当前选中的目录是否新的工作空间
        /// </summary>
        /// <param name="selectedPath"></param>
        /// <returns></returns>
        private bool IsNewWorkspace()
        {
            var els = root.Elements("workspace").ToList();
            var result = from el in els where el.Attribute("id").Value == Convert.ToString(selectItem.Id) select el;
            return !result.Any();
        }

        /// <summary>
        /// 创建工作空间目录
        /// </summary>
        private void CreateWorkspaceFolder()
        {
            // 工作空间目录不存在则创建
            IOUtil.CreateFolderIfNotExist(selectItem.Path);
            IOUtil.CreateFolderIfNotExist(selectItem.Path + CommonConst.FILE_separator + CommonConst.WORKSPACE_FOLER_NAME);

            // 暂存工作空间目录
            WorkspaceViewModel workspaceViewModel = IocUtil.getLocator().WorkspaceViewModel;
            workspaceViewModel.WorkspaceModel.WorkspacePath = selectItem.Path;
        }

        /// <summary>
        /// 复制配置
        /// </summary>
        private void CopySettings()
        {
            // Launcher 为 NEW 配置文件不存在则复制
            string workspaceSetting;
            if ( launcherType == LauncherType.NEW)
            {
                workspaceSetting = selectItem.Path + CommonConst.FILE_separator + CommonConst.WORKSPACE_FOLER_NAME + CommonConst.FILE_separator + CommonConst.PREFERENCE_FILE_NAME;
                if (!IOUtil.IsFileExist(workspaceSetting))
                {
                    IOUtil.copyFile(CommonConst.DEFAULT_SETTING_PATH, workspaceSetting);
                }
            }
            else
            {
                // Launcher 为 SWITCH
                workspaceSetting = launchSwitchWorkspath + CommonConst.FILE_separator + CommonConst.WORKSPACE_FOLER_NAME + CommonConst.FILE_separator + CommonConst.PREFERENCE_FILE_NAME;
                if( copyPerferences == true)
                {
                    IOUtil.copyFile(CommonConst.DEFAULT_SETTING_PATH, workspaceSetting);
                }
                else
                {
                    if (!IOUtil.IsFileExist(workspaceSetting))
                    {
                        IOUtil.copyFile(CommonConst.DEFAULT_SETTING_PATH, workspaceSetting);
                    }
                }
                
            }
        }

        /// <summary>
        /// 更改加载模式
        /// </summary>
        /// <param name="obj"></param>
        private void ChangeLaunchTypeMsg(NotificationMessage<SwitchWorkspaceDto> obj)
        {
            SwitchWorkspaceDto dto = obj.Content;
            launcherType = dto.LauncherType;
            currMainWindow = dto.MainWindow;

            copySettingShow = launcherType == LauncherType.SWITCH;
            
            if(launcherType == LauncherType.SWITCH)
            {
                copySettingShow = true;
                userAsDefaultShow = false;
                launchSwitchWorkspath = selectItem.Path;
            }
            else
            {
                copySettingShow = false;
                userAsDefaultShow = true;
            }

            RaisePropertyChanged("CopySettingShow");
            RaisePropertyChanged("UserAsDefaultShow");
        }

        #endregion Messenger

        #region Property

        private XElement root;

        private int lastId;

        public int LastId
        {
            get { return lastId; }
            set
            {
                lastId = value;
                RaisePropertyChanged(() => LastId);
            }
        }

        private LauncherModel selectItem;

        public LauncherModel SelectItem
        {
            get { return selectItem; }
            set
            {
                selectItem = value;
                RaisePropertyChanged(() => SelectItem);
            }
        }

        private ObservableCollection<LauncherModel> itemList;


        public ObservableCollection<LauncherModel> ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                RaisePropertyChanged(() => ItemList);
                RaisePropertyChanged("");
            }
        }

        private bool copySettingShow;

        public bool CopySettingShow
        {
            get
            {
                return copySettingShow;
            }
            set
            {
                copySettingShow = value;
                RaisePropertyChanged(() => CopySettingShow);
            }
        }

        private bool userAsDefaultShow;

        public bool UserAsDefaultShow
        {
            get
            {
                return userAsDefaultShow;
            }
            set
            {
                userAsDefaultShow = value;
                RaisePropertyChanged(() => UserAsDefaultShow);
            }
        }

        private bool? copyPerferences;
        public bool? CopyPerferences
        {
            get
            {
                return copyPerferences;
            }
            set
            {
                copyPerferences = value;
                RaisePropertyChanged(() => UserAsDefaultShow);
            }
        }




        private LauncherType launcherType;

        private string launchSwitchWorkspath;

        private MainWindow currMainWindow;

        #endregion Property
    }

    /// <summary>
    /// 加载模式
    /// </summary>
    public enum LauncherType
    {
        /// <summary>
        /// 新的应用实例
        /// </summary>
        NEW,
        /// <summary>
        /// 选择其他工作空间
        /// </summary>
        SWITCH
    }
}
