﻿using blocks_editor.src.consts;
using blocks_editor.src.dto;
using blocks_editor.src.util;
using blocks_editor.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using System.Linq;
using System.Xml.Linq;

namespace blocks_editor
{
    /// <summary>
    /// Launcher.xaml 的交互逻辑
    /// </summary>
    public partial class Launcher : Window
    {
        public Launcher()
        {
            InitializeComponent();

            RegisterMessenger();
        }

        private void RegisterMessenger()
        {
            Messenger.Default.Register<NotificationMessage<OpenMainWindowDto>>(this, MessengerConst.App.APP_LAUNCH_MAIN_WINDOW, LaunchMainWindowMsg);
        }

        #region messenger
        private void LaunchMainWindowMsg(NotificationMessage<OpenMainWindowDto> msg)
        {
            OpenMainWindowDto dto = msg.Content;
            string selectPath = dto.WorkspacePath;
            if(dto.LauncherType == LauncherType.SWITCH)
            {
                //https://cloud.tencent.com/developer/article/1669769
                //System.Environment.Exit(0);
                //System.Diagnostics.Process.GetCurrentProcess().Kill();
                XElement root = XElement.Load(CommonConst.WORKSPACE_DATA_PATH);
                // 修改加载模式
                var els = from el in root.Elements("launch") select el;
                els.ToArray()[0].SetAttributeValue("mode", "SWITCH");
                els.ToArray()[0].SetAttributeValue("path", selectPath);

                // 保存文件
                root.Save(CommonConst.WORKSPACE_DATA_PATH);

                //dto.MainWindow.Close();
                //System.Windows.Forms.Application.Restart();
                //Application.Current.Shutdown();

                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }
            else
            {
                MainWindow mainWindow = new MainWindow(selectPath);
                mainWindow.Show();
                mainWindow.ShowActivated = true;
            }
            Close();
        }
        #endregion messenger

        #region click
        private void Click_browse_workspace_path(object sender, RoutedEventArgs e)
        {
            CommonUtil.BrowserPath("Select Workspace Dierectory", "", (p) =>
            {
                Messenger.Default.Send(p, MessengerConst.Launcher.SELECT_WORKSPACE);
            });
        }

        private void Click_launch_workspace(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(MessengerConst.Launcher.LAUNCH_WORKSPACE, MessengerConst.Launcher.LAUNCH_WORKSPACE);
        }
        #endregion click
    }
}
