﻿using blocks_editor.src.util;
using blocks_editor.ViewModel;
using System.Windows;
using System.Linq;
using System.Xml.Linq;
using blocks_editor.src.consts;
using blocks_editor.ViewModel.dialog;

namespace blocks_editor
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            InitViewModel();
            Launch();
        }

        private void InitViewModel()
        {
            LauncherViewModel = IocUtil.getLocator().LauncherViewModel;
            NewWorkingSetViewModel = IocUtil.getLocator().NewWorkingSetViewModel;

            SelectWorkingSetViewModel = IocUtil.getLocator().SelectWorkingSetViewModel;
        }

        private void Launch()
        {
            // 读取加载模式
            XElement root = XElement.Load(CommonConst.WORKSPACE_DATA_PATH);
            var els = from el in root.Elements("launch") select el;
            var launchEl = els.ToArray()[0];
            if ("NEW" == launchEl.Attribute("mode").Value)
            {
                Launcher launcher = new Launcher();
                launcher.Show();
            }
            else
            {

                els = from el in root.Elements("last") select el;
                var lastEl = els.ToArray()[0];

                string workspacePath = launchEl.Attribute("path").Value;
                els = from el in root.Elements("workspace") select el;
                if (els.Any())
                {
                    foreach (var el in els)
                    {
                        if( el.Attribute("path").Value == workspacePath)
                        {
                            lastEl.SetAttributeValue("id", el.Attribute("id").Value);
                            break;
                        }
                    }
                }

                launchEl.SetAttributeValue("mode","NEW");

                root.Save(CommonConst.WORKSPACE_DATA_PATH);
                MainWindow mainWindow = new MainWindow(workspacePath);
                mainWindow.Show();
                
            }
            
        }

        #region app
        public SelectWorkingSetViewModel SelectWorkingSetViewModel { get; set; }
        public NewWorkingSetViewModel NewWorkingSetViewModel { get; set; }
        #endregion app


        #region Launcher
        public LauncherViewModel LauncherViewModel { get; set; }
        #endregion Launcher
    }
}
