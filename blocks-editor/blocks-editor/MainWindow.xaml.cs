﻿using blocks_editor.src.consts;
using blocks_editor.src.exception;
using blocks_editor.src.model;
using blocks_editor.src.util;
using log4net;
using System;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;
using GalaSoft.MvvmLight.Messaging;
using blocks_editor.ViewModel;
using blocks_editor.src.dto;

namespace blocks_editor
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly static ILog log = LogManager.GetLogger(typeof(MainWindow));
        private string currWorkspacePath;

        public MainWindow()
        {
            InitializeComponent();

            Init();
        }

        public MainWindow(string currWorkspacePath)
        {
            InitializeComponent();

            this.currWorkspacePath = currWorkspacePath;
            UpdateTitlte();

            Init();
        }

        #region init

        private void Init()
        {
            UpdateSwitchWorkspaceMenuItem();
        }

        /// <summary>
        /// 更新 File -> Switch Workspace
        /// </summary>
        private void UpdateSwitchWorkspaceMenuItem()
        {
            ItemCollection items = menu_file_switch_workspace.Items;

            List<WorkspaceModel> list = LoadWorkspaceList();
            if (list.Count > 0)
            {
                foreach (WorkspaceModel model in list)
                {
                    if (currWorkspacePath != model.WorkspacePath)
                    {
                        string label = model.WorkspacePath.Replace("_","__");
                        MenuItem item = new MenuItem
                        {
                            Header = label
                        };
                        items.Insert(0, item);
                    }
                }
            }
        }

        /// <summary>
        /// 加载所有的工作空间路径
        /// </summary>
        /// <returns></returns>
        private List<WorkspaceModel> LoadWorkspaceList()
        {
            List<WorkspaceModel> list = new List<WorkspaceModel>();

            XElement root = XElement.Load(CommonConst.WORKSPACE_DATA_PATH);
            try
            {
                var els = from el in root.Elements("workspace") select el;
                if (els.Any())
                {
                    foreach (var el in els)
                    {
                        list.Add(new WorkspaceModel
                        {
                            WorkspacePath = el.Attribute("path").Value
                        });
                    }
                }
            }
            catch (BussinessException e)
            {
                log.Error(StrUtil.format("加载工作空间出错", e.Message));
            }

            return list;
        }

        #endregion init



        #region layout
        private void Icon_enable_changed(object sender, DependencyPropertyChangedEventArgs e)
        {
            IconGrayHandle((Image)sender);
        }

        /// <summary>
        /// 图片灰度处理
        /// </summary>
        /// <param name="icon">图标对象</param>
        private void IconGrayHandle(Image icon)
        {
            if (null != icon && null != icon.Source)
            {

                // 原图片地址
                string iconUrlOriginal = icon.Source.ToString();
                string suffix = IOUtil.getFileSuffix(iconUrlOriginal);
                // "pack://application:,,,/resources/icon/save_unable.png"
                BitmapImage bmp = new BitmapImage();
                try
                {
                    bmp.BeginInit();

                    string newUrl;
                    // 修改为非灰度图片, 去除图片地址后面的 _unable
                    if (icon.IsEnabled)
                    {
                        newUrl = iconUrlOriginal.Substring(0, iconUrlOriginal.Length - 11) + suffix;
                    }
                    else
                    {
                        // 如果以 _unable.png 结尾,则说明是初始化时即为灰度图片
                        if (iconUrlOriginal.EndsWith("_unable" + suffix))
                        {
                            newUrl = iconUrlOriginal;
                        }
                        else
                        {
                            newUrl = iconUrlOriginal.Substring(0, iconUrlOriginal.Length - 4) + "_unable" + suffix;
                        }
                    }
                    bmp.UriSource = new Uri(newUrl);
                    bmp.EndInit();
                }
                catch (BussinessException e)
                {
                    log.Error(e.Message);
                    // 如果发生异常,则复原为原图片地址
                    bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.UriSource = new Uri(iconUrlOriginal);
                    bmp.EndInit();
                }
                icon.Source = bmp;
            }
        }

        /// <summary>
        /// 工具栏按钮灰度处理
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Toolbar_btn_enabled_changed(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (sender is Button)
                {
                    Button btn = sender as Button;
                    btn.Opacity = btn.IsEnabled ? 1 : 0.5;
                }

                if (sender is SplitButton)
                {
                    SplitButton btn = sender as SplitButton;
                    btn.Opacity = btn.IsEnabled ? 1 : 0.5;
                }
            }
            catch (BussinessException ex)
            {
                log.Error(ex.Message);
            }
        }

        /// <summary>
        /// 去除工具栏右侧三角
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ToolBar_Loaded(object sender, RoutedEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;
            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }

            var mainPanelBorder = toolBar.Template.FindName("MainPanelBorder", toolBar) as FrameworkElement;
            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness(0);
            }
        }
        #endregion layout

        #region property

        /// <summary>
        /// 当前工作空间路径
        /// </summary>
        public string CurrWorkspacePath
        {
            get
            {
                return currWorkspacePath;
            }
            set
            {
                currWorkspacePath = value;
                UpdateTitlte();
            }
        }

        #endregion property

        #region method

        /// <summary>
        /// 更新标题
        /// </summary>
        private void UpdateTitlte()
        {
            int index = currWorkspacePath.LastIndexOf("\\");
            Title = currWorkspacePath.Substring(index + 1, currWorkspacePath.Length - index - 1) + " - " + Title;
        }

        #endregion method

        #region menu_click

        /// <summary>
        /// TODO 选择其他工作空间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItemClick_switch_workspace_other(object sender, RoutedEventArgs e)
        {
            Launcher launcher = new Launcher();
            Messenger.Default.Send(new NotificationMessage<SwitchWorkspaceDto>(new SwitchWorkspaceDto
            {
                LauncherType = LauncherType.SWITCH,
                MainWindow = this
            }, MessengerConst.Launcher.CHANGE_LAUNCH_TYPE), MessengerConst.Launcher.CHANGE_LAUNCH_TYPE);
            launcher.ShowInTaskbar = false;
            launcher.ShowDialog();
        }

        #endregion menu_click
    }
}
