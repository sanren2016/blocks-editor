﻿using blocks_editor.src.consts;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace blocks_editor.ui.dialog
{
    /// <summary>
    /// SelectWorkingSet.xaml 的交互逻辑
    /// </summary>
    public partial class SelectWorkingSetDialog : Window
    {
        public SelectWorkingSetDialog()
        {
            InitializeComponent();
        }

        #region click

        /// <summary>
        /// 打开新建 working set 对话框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void New_working_set_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send<string>(MessengerConst.NewWorkingSet.OPEN_NEW_WORKING_SET, MessengerConst.NewWorkingSet.OPEN_NEW_WORKING_SET);
        }

        #endregion click
    }
}
