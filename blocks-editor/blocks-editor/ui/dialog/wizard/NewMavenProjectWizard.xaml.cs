﻿using blocks_editor.src.consts;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace blocks_editor.ui.dialog.wizard
{
    /// <summary>
    /// NewMavenProjectWizard.xaml 的交互逻辑
    /// </summary>
    public partial class NewMavenProjectWizard : Window
    {
        public NewMavenProjectWizard()
        {
            InitializeComponent();
        }

        #region click

        private void ShowLocationPanel()
        {
            locationPanel.Visibility = Visibility.Visible;
            infoPanel.Visibility = Visibility.Collapsed;

            btnBack.IsEnabled = false;
            btnNext.IsEnabled = true;
        }

        private void ShowInfoPanel()
        {
            locationPanel.Visibility = Visibility.Collapsed;
            infoPanel.Visibility = Visibility.Visible;

            btnBack.IsEnabled = true;
            btnNext.IsEnabled = false;
        }

        /// <summary>
        /// Back
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            ShowLocationPanel();
        }

        /// <summary>
        /// Next
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            ShowInfoPanel();
        }

        /// <summary>
        /// Finish
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnFinish_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SelectWorkingset_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(MessengerConst.SelectWorkingSet.OPEN_SELECT_WORKING_SET, MessengerConst.SelectWorkingSet.OPEN_SELECT_WORKING_SET);
        }

        
        #endregion click

        

        
    }
}
