﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace blocks_editor.ui.control.perspective
{
    /// <summary>
    /// PackageExplorerPanelControl.xaml 的交互逻辑
    /// </summary>
    public partial class PackageExplorerPanelControl : UserControl
    {
        public PackageExplorerPanelControl()
        {
            InitializeComponent();
        }

        private void ViewMenu_Click(object sender, RoutedEventArgs e)
        {
            viewmenu_ctxmenu.PlacementTarget = btn_viewmenu;
            viewmenu_ctxmenu.Placement = PlacementMode.Top;
            viewmenu_ctxmenu.IsOpen = true;
        }

        private void ViewMenu_Initialized(object sender, System.EventArgs e)
        {
            btn_viewmenu.ContextMenu = null;
        }

        private void Create_Java_Project_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("Create Project","debug");
        }
        private void Create_Spring_Project_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("Create Project", "debug");
        }
        private void Import_Spring_Project_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("Create Project", "debug");
        }
        private void Create_Project_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("Create Project", "debug");
        }
        private void Import_Project_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("Create Project", "debug");
        }
    }
}
