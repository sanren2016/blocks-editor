﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace blocks_editor.ui.control
{
    /// <summary>
    /// CheckboxWithIcon.xaml 的交互逻辑
    /// </summary>
    public partial class CheckboxWithIcon : UserControl , INotifyPropertyChanged
    {
        public CheckboxWithIcon()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string prop)
        {
            if (null != prop && IsLoaded)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        #endregion INotifyPropertyChanged

        #region property
        /* label */
        public static readonly DependencyProperty LabelContentProperty = DependencyProperty.Register("LabelContent",
            typeof(string), typeof(CheckboxWithIcon), new PropertyMetadata("", new PropertyChangedCallback(OnLabelContentPropertyChange)));

        private static void OnLabelContentPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckboxWithIcon checkbox = d as CheckboxWithIcon;
            Label label = checkbox.FindName("label") as Label;
            label.Content = e.NewValue.ToString();
        }

        public string LabelContent
        {
            get { return (string)GetValue(LabelContentProperty); }
            set { SetValue(LabelContentProperty, value); }
        }

        /* 图片地址 */
        public static readonly DependencyProperty IconPathProperty = DependencyProperty.Register("IconPath",
        typeof(string), typeof(CheckboxWithIcon), new PropertyMetadata("", new PropertyChangedCallback(OnIconPathPropertyChange)));

        private static void OnIconPathPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckboxWithIcon checkbox = d as CheckboxWithIcon;
            Image icon = checkbox.FindName("icon") as Image;
            icon.Source = new BitmapImage(new Uri("pack://application:,,," + e.NewValue.ToString()));
        }

        public string IconPath
        {
            get { return (string)GetValue(IconPathProperty); }
            set { SetValue(IconPathProperty, value); }
        }

        /* 复选框 */
        public static readonly DependencyProperty ControlCheckedCheckedProperty = DependencyProperty.Register("ControlChecked",
            typeof(string), typeof(CheckboxWithIcon), new PropertyMetadata("", new PropertyChangedCallback(OnControlCheckedPropertyChange)));

        private static void OnControlCheckedPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckboxWithIcon checkbox = d as CheckboxWithIcon;
            checkbox.CheckBoxChecked = Convert.ToBoolean(e.NewValue);
        }

        public string ControlChecked
        {
            get { return (string)GetValue(ControlCheckedCheckedProperty); }
            set { SetValue(ControlCheckedCheckedProperty, value); }
        }

        private bool? checkBoxChecked = false;
        public bool? CheckBoxChecked
        {
            get
            {
                return checkBoxChecked;
            }
            set
            {
                checkBoxChecked = value;
                RaisePropertyChanged("CheckBoxChecked");
            }
        }
        #endregion property
    }
}
