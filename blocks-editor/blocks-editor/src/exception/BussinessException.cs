﻿using System;

namespace blocks_editor.src.exception
{
    public class BussinessException : Exception
    {
        public BussinessException() : base("未知错误")
        {
        }

        public BussinessException(string msg) : base(msg)
        {
        }

        public BussinessException(int code, string msg) : base(msg)
        {
            Code = code;
        }

        public int Code { get; set; }
    }
}
