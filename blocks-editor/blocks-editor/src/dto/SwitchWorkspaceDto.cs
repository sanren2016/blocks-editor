﻿using blocks_editor.ViewModel;

namespace blocks_editor.src.dto
{
    public class SwitchWorkspaceDto
    {
        public LauncherType LauncherType { get; set; }

        public MainWindow MainWindow { get; set; }
    }
}
