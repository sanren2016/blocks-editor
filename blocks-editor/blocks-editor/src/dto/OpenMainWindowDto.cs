﻿using blocks_editor.ViewModel;

namespace blocks_editor.src.dto
{
    class OpenMainWindowDto
    {
        public string WorkspacePath { get; set; }
        public MainWindow MainWindow { get; set; }
        public LauncherType LauncherType { get; set; }
    }
}
