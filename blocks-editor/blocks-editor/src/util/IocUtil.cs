﻿using blocks_editor.ViewModel;
using System.Windows;

namespace blocks_editor.src.util
{
    public class IocUtil
    {
        private IocUtil()
        {
        }

        public static ViewModelLocator getLocator()
        {
            return Application.Current.Resources["Locator"] as ViewModelLocator;
        }
    }
}
