﻿using System.Text;

namespace blocks_editor.src.util
{
    public class StrUtil
    {
        private StrUtil() { }

        private const string EMPTY_JSON = "{}";

        private const string C_BACKSLASH = "\\";

        private const string C_DELIM_START = "{";

        /// <summary>
        /// 此方法只是简单将占位符 {} 按照顺序替换为参数
        /// 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可
        /// 例：
        /// 通常使用：format("this is {} for {}", "a", "b") =》 this is a for b
        /// 转义{}： format("this is \\{} for {}", "a", "b") =》 this is \{} for a
        /// 转义\： format("this is \\\\{} for {}", "a", "b") =》 this is \a for b
        /// </summary>
        /// <param name="strPattern">文本模板，被替换的部分用 {} 表示</param>
        /// <param name="values">参数值</param>
        /// <returns>格式化后的文本</returns>
        public static string format(string strPattern, params object[] values)
        {
            if (isBlank(strPattern))
            {
                return null;
            }
            if (0 == values.Length)
            {
                return strPattern;
            }
            int strPatternLength = strPattern.Length;
            StringBuilder sbuf = new StringBuilder();
            // 记录已经处理到的位置
            int handledPosition = 0;
            // 占位符所在位置
            int delimIndex;
            for (int argIndex = 0; argIndex < values.Length; argIndex++)
            {
                delimIndex = strPattern.IndexOf(EMPTY_JSON, handledPosition);
                // 剩余部分无占位符
                if (-1 == delimIndex)
                {
                    // 不带占位符的模板直接返回
                    if (0 == handledPosition)
                    {
                        return strPattern;
                    }
                    // 字符串模板剩余部分不再包含占位符,加入剩余部分后返回结果
                    sbuf.Append(strPattern.Substring(handledPosition, strPattern.Length - handledPosition));
                    return sbuf.ToString();
                }

                // 转义符
                if (delimIndex > 0 && charAt(strPattern, delimIndex - 1) == C_BACKSLASH)
                {
                    // 双转义符
                    if (delimIndex > 1 && charAt(strPattern, delimIndex - 2) == C_BACKSLASH)
                    {
                        // 转义符之前还有一个转义符,占位符依旧有效
                        sbuf.Append(strPattern.Substring(handledPosition, delimIndex - 1 - handledPosition));
                        sbuf.Append(values[argIndex].ToString());
                        handledPosition = delimIndex + 2;

                    }
                    else
                    {
                        // 占位符被转义
                        argIndex--;
                        sbuf.Append(strPattern.Substring(handledPosition, delimIndex - 1 - handledPosition));
                        sbuf.Append(C_DELIM_START);
                        handledPosition = delimIndex + 1;
                    }
                }
                else
                {
                    // 正常占位符
                    sbuf.Append(strPattern.Substring(handledPosition, delimIndex - handledPosition));
                    sbuf.Append(values[argIndex].ToString());
                    handledPosition = delimIndex + 2;
                }
            }

            // 加入最后徐一个占位符后所有的字符
            sbuf.Append(strPattern.Substring(handledPosition, strPattern.Length - handledPosition));

            return sbuf.ToString();
        }

        public static bool isEmpty(string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool isNotEmpty(string str)
        {
            return !isEmpty(str);
        }

        public static bool isAllEmpty(params string[] strs)
        {
            if (0 == strs.Length)
            {
                return true;
            }
            foreach (string str in strs)
            {
                if (isNotEmpty(str))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool isBlank(string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool isNotBlank(string str)
        {
            return !isBlank(str);
        }

        public static bool isAllBlank(params string[] strs)
        {
            if (0 == strs.Length)
            {
                return true;
            }
            foreach (string str in strs)
            {
                if (isNotBlank(str))
                {
                    return false;
                }
            }
            return true;
        }

        public static string charAt(string str, int index)
        {
            if (isEmpty(str))
            {
                return null;
            }
            if (index >= str.Length || index < 0)
            {
                return null;
            }

            return str.Substring(index, 1);

        }
    }
}
