﻿namespace blocks_editor.src.util
{
    /// <summary>
    /// IO工具类抽取
    /// </summary>
    public class IOUtil
    {
        private IOUtil() { }

        /// <summary>
        /// 获取文件名后缀,无则返回空字符串
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public static string getFileSuffix(string fileName)
        {

            int dotIndex = fileName.LastIndexOf(".");
            if (-1 == dotIndex)
            {
                return "";
            }
            return fileName.Substring(dotIndex);
        }

        /// <summary>
        /// 目录不存在则创建
        /// </summary>
        /// <param name="path"></param>
        public static void CreateFolderIfNotExist(string path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool IsFileExist(string path)
        {
            return System.IO.File.Exists(path);
        }

        /// <summary>
        /// 复制文件
        /// </summary>
        /// <param name="src">源文件路径</param>
        /// <param name="dest">目标文件路径</param>
        public static void copyFile(string src, string dest)
        {
            System.IO.File.Copy(src, dest, true);
        }

        /// <summary>
        /// 复制文件
        /// </summary>
        /// <param name="src">源文件路径</param>
        /// <param name="dest">目标文件路径</param>
        /// <param name="replace">如果目标文件存在,是否替换</param>
        public static void copyFile(string src, string dest, bool replace)
        {
            System.IO.File.Copy(src, dest, replace);
        }

    }
}
