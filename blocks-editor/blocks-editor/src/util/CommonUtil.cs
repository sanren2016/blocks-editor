﻿using blocks_editor.src.consts;
using blocks_editor.src.exception;
using log4net;
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace blocks_editor.src.util
{
    public class CommonUtil
    {
        readonly static ILog log = LogManager.GetLogger(typeof(CommonUtil));

        private CommonUtil() { }


        /// <summary>
        /// 字符串转bool
        /// </summary>
        /// <param name="boolstr"></param>
        /// <returns></returns>
        public static bool String2bool(string boolstr)
        {
            if ("True" == boolstr)
            {
                return true;
            }
            if ("False" == boolstr)
            {
                return false;
            }
            throw new BussinessException(StrUtil.format("字符串转换bool失败,str:{}", boolstr));
        }

        public static string BuildProjectSettingPath(string projectPath)
        {
            return projectPath + CommonConst.FILE_separator + ".project";
        }

        #region dialog
        /// <summary>
        /// 选择目录
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="selectedPath">从哪个目录开始选择</param>
        /// <param name="okAction">确定动作</param>
        /// <param name="cancelAction">取消动作</param>
        public static void BrowserPath(string title, string selectedPath, Action<string> okAction, Action<string> cancelAction)
        {
            FolderBrowserDialog fbWnd = new FolderBrowserDialog();
            // 目录对话框的描述字符串
            fbWnd.Description = title;
            // 自定义从哪个目录下面选择
            fbWnd.SelectedPath = selectedPath;
            // 是否显示目录对话框左下角的“新建文件夹”按钮， true：显示， false： 隐藏
            fbWnd.ShowNewFolderButton = true;
            // 目录选择完之后是点击确认还是点击取消, 确认： OK， 取消： Cancel
            string strbtn = fbWnd.ShowDialog().ToString();

            if (strbtn.CompareTo("OK") == 0)
            {
                okAction(fbWnd.SelectedPath);
            }

            if (strbtn.CompareTo("Cancel") == 0)
            {
                cancelAction(fbWnd.SelectedPath);
            }
        }

        /// <summary>
        /// 选择目录
        /// </summary>
        public static void BrowserPath(string title, string selectedPath, Action<string> okAction)
        {
            BrowserPath(title, selectedPath, okAction, (p) => { });
        }
        #endregion dialog

        #region 从文件加载默认配置
        /// <summary>
        /// 从默认配置文件查找节点
        /// </summary>
        /// <param name="path">路径,以 . 分割</param>
        /// <returns></returns>
        public static XElement GetPreferencesXElement(string path)
        {
            // 空字符串返回 null
            if (StrUtil.isBlank(path))
            {
                return null;
            }


            string errorTpl = "加载首选项错误, 查找路径为:[{}] , 查找节点:[{}] 失败";
            string pathFailed = "";
            Action<string[], int> pathFailedAction = (strs, index) =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (string str in strs)
                {
                    sb.Append(str).Append(".");
                }
                pathFailed = sb.ToString();
                pathFailed = pathFailed.Substring(0, pathFailed.Length - 1);
            };

            XElement root = GetPreferencesXmlRoot();
            if (null != root)
            {
                string[] steps = path.Split('.');
                // 查找第一层
                var els = from el in root.Elements(XName.Get(steps[0])) select el;
                if (els.Any())
                {
                    // 如果第一层找到切path只有一层,直接返回
                    if (1 == steps.Length)
                    {
                        return els.ToArray()[0];
                    }

                    // 查找其他层
                    for (int i = 1; i < steps.Length; i++)
                    {
                        els = from el in els.Elements(XName.Get(steps[i])) select el;
                        // 未找到则直接退出
                        if (!els.Any())
                        {
                            pathFailedAction(steps, i);
                            throw new BussinessException(StrUtil.format(errorTpl, path, pathFailed));
                        }
                    }

                    // 查找完毕
                    return els.ToArray()[0];
                }
                else
                {
                    pathFailedAction(steps, steps.Length);
                    throw new BussinessException(StrUtil.format(errorTpl, path, pathFailed));
                }

            }
            return null;
        }

        private static XElement GetPreferencesXmlRoot()
        {
            XElement root = null;
            try
            {
                root = XElement.Load(CommonConst.DEFAULT_SETTING_PATH);
            }
            catch (Exception e)
            {
                log.Error(StrUtil.format("加载默认配置失败:{}", e.Message));
            }
            return root;
        }
        #endregion


    }
}
