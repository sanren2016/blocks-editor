﻿namespace blocks_editor.src.consts
{
    public class CommonConst
    {

        private CommonConst() { }

        public const string BOOL_STRING_TRUE = "true";
        public const string BOOL_STRING_FALSE = "false";

        /// <summary>
        /// 文件分隔符
        /// </summary>
        public const string FILE_separator = "\\";

        /// <summary>
        /// 默认配置文件路径
        /// </summary>
        public const string DEFAULT_SETTING_PATH = "./resources/settings/preferences_default.xml";

        /// <summary>
        /// 配置文件名称
        /// </summary>
        public const string PREFERENCE_FILE_NAME = "preferences.xml";

        /// <summary>
        /// 工作空间目录名称
        /// </summary>
        public const string WORKSPACE_FOLER_NAME = ".workspace";


        /// <summary>
        /// 工作空间保存路径
        /// </summary>
        public const string WORKSPACE_DATA_PATH = "./resources/data/workspace.xml";


    }
}
