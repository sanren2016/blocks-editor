﻿namespace blocks_editor.src.consts
{
    /// <summary>
    /// mvvmlight messenger
    /// </summary>
    public class MessengerConst
    {
        protected MessengerConst() { }


        public class App
        {
            protected App() { }

            /// <summary>
            /// 加载主窗口
            /// </summary>
            public const string APP_LAUNCH_MAIN_WINDOW = "app_launch_main_window";
        }

        public class Launcher
        {
            protected Launcher() { }

            /// <summary>
            /// 加载工作空间
            /// </summary>
            public const string LAUNCH_WORKSPACE = "launch_workspace";

            /// <summary>
            /// 选择工作空间目录
            /// </summary>
            public const string SELECT_WORKSPACE = "select_workspace";

            /// <summary>
            /// 修改加载模式
            /// </summary>
            public const string CHANGE_LAUNCH_TYPE = "change_launch_type";
        }

        public class SelectWorkingSet
        {
            protected SelectWorkingSet() { }

            /// <summary>
            /// 打开 working set 对话框
            /// </summary>
            public const string OPEN_SELECT_WORKING_SET = "open_select_working_set";
        }

        public class NewWorkingSet
        {
            protected NewWorkingSet() { }

            /// <summary>
            /// 打开 新建 working set 对话框
            /// </summary>
            public const string OPEN_NEW_WORKING_SET = "open_new_working_set";
        }
    }
}
