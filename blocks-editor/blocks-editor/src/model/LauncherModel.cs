﻿using GalaSoft.MvvmLight;

namespace blocks_editor.src.model
{
    public class LauncherModel : ObservableObject
    {


        public LauncherModel(string path, int id)
        {
            Path = path;
            Id = id;
        }

        public LauncherModel()
        {
        }

        private string path;
        public string Path { get { return path; } set { path = value; RaisePropertyChanged(() => Path); } }

        private int id;
        public int Id { get { return id; } set { id = value; RaisePropertyChanged(() => Id); } }
    }
}
