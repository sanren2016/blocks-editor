﻿using blocks_editor.src.model.perspective.packageexplorer.bean.java.enums;

namespace blocks_editor.src.model.perspective.packageexplorer.bean.java.bases
{
    public class BaseJavaProject : BaseProject
    {
        protected string jdkVersion;
        protected string jdkVersionDetail;
        protected JavaBuildToolType buildToolType;

        /// <summary>
        /// jdk版本
        /// </summary>
        public string JdkVersion { get => jdkVersion; set => jdkVersion = value; }

        /// <summary>
        /// jdk详细版本
        /// </summary>
        public string JdkVersionDetail { get => jdkVersionDetail; set => jdkVersionDetail = value; }

        /// <summary>
        /// 构建工具类型
        /// </summary>
        public JavaBuildToolType BuildToolType { get => buildToolType; set => buildToolType = value; }
    }
}
