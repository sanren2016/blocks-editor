﻿using blocks_editor.src.model.perspective.packageexplorer.bean.java.bases;

namespace blocks_editor.src.model.perspective.packageexplorer.bean.java.maven
{
    public class MavenProject : BaseJavaProject
    {
        private string mvnVersion;
        private string mvnVersionDetail;

        /// <summary>
        /// maven版本
        /// </summary>
        public string MvnVersion { get => mvnVersion; set => mvnVersion = value; }

        /// <summary>
        /// maven详细版本
        /// </summary>
        public string MvnVersionDetail { get => mvnVersionDetail; set => mvnVersionDetail = value; }
    }
}
