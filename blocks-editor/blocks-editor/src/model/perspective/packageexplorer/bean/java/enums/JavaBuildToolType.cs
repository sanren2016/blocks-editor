﻿namespace blocks_editor.src.model.perspective.packageexplorer.bean.java.enums
{
    /// <summary>
    /// java 构建工具类型
    /// </summary>
    public enum JavaBuildToolType
    {
        Ant = 1,
        Maven = 2,
        Gradle = 3
    }
}
