﻿using System.Collections.Generic;

namespace blocks_editor.src.model.perspective.packageexplorer.bean
{
    public abstract class BaseProject
    {
        protected string name;
        protected string location;
        protected List<string> workingsetList;


        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// 项目路径
        /// </summary>
        public string Location { get => location; set => location = value; }

        /// <summary>
        /// 项目所在 workingset 集合
        /// </summary>
        public List<string> WorkingsetList { get => workingsetList; set => workingsetList = value; }
    }
}
