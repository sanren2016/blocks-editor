﻿using GalaSoft.MvvmLight;

namespace blocks_editor.src.model
{
    public class WorkspaceModel : ObservableObject
    {
        private string workspacePath;

        public string WorkspacePath
        {
            get => workspacePath;
            set
            {
                workspacePath = value;
                RaisePropertyChanged(() => WorkspacePath);
            }
        }
    }
}
